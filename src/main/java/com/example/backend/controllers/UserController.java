package com.example.backend.controllers;

import com.example.backend.entities.Role;
import com.example.backend.entities.User;
import com.example.backend.services.UserServices;
import jdk.jfr.DataAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserServices userServices;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllU(){
        return ResponseEntity.ok().body( userServices.getAllUsers());
    }

    @PostMapping("/signup")
    public ResponseEntity<User> createUser(@RequestBody User userEntity){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/sigup").toUriString());
        return ResponseEntity.created(uri).body(this.userServices.createUser(userEntity));
    }

    @PostMapping("users/roles")
    public ResponseEntity<Role> saveRole(@RequestBody Role roleEntity){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/users/roles").toUriString());
        return ResponseEntity.created(uri).body(this.userServices.saveRole(roleEntity));
    }

    @PostMapping("users/addtouser")
    public ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserForm form){
        userServices.addRole(form.getEmail(), form.getRoleName());
        return ResponseEntity.ok().build();
    }

}

class RoleToUserForm {
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    private String email;
    private String roleName;
}


package com.example.backend.controllers;


import com.example.backend.entities.Campaing;
import com.example.backend.entities.User;
import com.example.backend.services.CampaingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class CampainControllers {

    CampaingService campaingService;

    @GetMapping("/camps")
    public ResponseEntity<List<Campaing>> getAllU(){
        return ResponseEntity.ok().body( campaingService.listCampaings());
    }

    @PostMapping("/camp")
    public ResponseEntity<Campaing> createUser(@RequestBody Campaing campaing){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/camp").toUriString());
        return ResponseEntity.created(uri).body(this.campaingService.saveCampaing(campaing));
    }
}

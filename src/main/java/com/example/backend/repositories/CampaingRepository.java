package com.example.backend.repositories;

import com.example.backend.entities.Campaing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CampaingRepository extends JpaRepository<Campaing, Long>{
    Optional<Campaing> findById(Long id);
}

package com.example.backend.services;

import com.example.backend.entities.Campaing;
import com.example.backend.entities.Role;
import com.example.backend.entities.User;

import java.util.List;

public interface IUser {
    User createUser(User user);
    List<User> getAllUsers();
    Role saveRole(Role role);
    void addRole(String name, String roleName);

}


package com.example.backend.services;

import com.example.backend.entities.Campaing;
import com.example.backend.repositories.CampaingRepository;

import java.util.List;

public class CampaingService implements ICampaigs{

    private final CampaingRepository campaingRepository;

    public CampaingService(CampaingRepository campaingRepository) {
        this.campaingRepository = campaingRepository;
    }

    @Override
    public Campaing saveCampaing(Campaing campaing) {
        return campaingRepository.save(campaing);
    }

    @Override
    public List<Campaing> listCampaings() {
        return campaingRepository.findAll();
    }
}

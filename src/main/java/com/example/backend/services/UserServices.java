package com.example.backend.services;
import com.example.backend.entities.Campaing;
import com.example.backend.entities.Role;
import com.example.backend.entities.User;
import com.example.backend.repositories.CampaingRepository;
import com.example.backend.repositories.RoleRepo;
import com.example.backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServices implements IUser{

    @Autowired
    private final UserRepository userRepository;
    private final RoleRepo roleRepo;
    private final CampaingRepository campaingRepository;

    public UserServices(UserRepository userRepository, RoleRepo roleRepo, CampaingRepository campaingRepository) {
        this.userRepository = userRepository;
        this.roleRepo = roleRepo;
        this.campaingRepository = campaingRepository;
    }

    @Override
    public User createUser(User userEntity) {
        return userRepository.save(userEntity);
    }

    @Override
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    @Override
    public Role saveRole(Role role) {
        return roleRepo.save(role);
    }

    @Override
    public void addRole(String email, String roleName) {
        User user = userRepository.findByEmail(email);
        Role role = roleRepo.findByName(roleName);
        user.getRoles().add(role);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }


}

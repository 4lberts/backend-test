package com.example.backend.entities;

import org.apache.juli.logging.Log;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "Campaings")
public class Campaing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String subjects;
    private Integer numberOfRecipients;
    private boolean status;

    @CreationTimestamp
    private Date createdAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    private User user;

    public Campaing(){}

    public Campaing(Long id, String subjects, Integer numberOfRecipients, boolean status, Date createdAt) {
        this.id = id;
        this.subjects = subjects;
        this.numberOfRecipients = numberOfRecipients;
        this.status = status;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public Integer getNumberOfRecipients() {
        return numberOfRecipients;
    }

    public void setNumberOfRecipients(Integer numberOfRecipients) {
        this.numberOfRecipients = numberOfRecipients;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;}
    }
